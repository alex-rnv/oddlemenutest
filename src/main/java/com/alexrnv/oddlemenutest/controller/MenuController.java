package com.alexrnv.oddlemenutest.controller;

import com.alexrnv.oddlemenutest.dao.MenuDAO;
import com.alexrnv.oddlemenutest.entity.Menu;
import com.alexrnv.oddlemenutest.entity.MenuResponse;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.slf4j.LoggerFactory.getLogger;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class MenuController  {
    private static final Logger LOG = getLogger(MenuController.class);

    private final MenuDAO menuDAO;

    public MenuController(MenuDAO menuDAO) {
        this.menuDAO = menuDAO;
    }

    @GET
    @Path("/get/{id}")
    @UnitOfWork
    public Response getAllMenu(@PathParam("id") int menuId) {
        MenuResponse menuResponse;
        try {
            Menu menu = menuDAO.findById(menuId);
            if(menu == null) {
                menuResponse = new MenuResponse("error", "resource not found", null);
                return Response.status(Response.Status.BAD_REQUEST).entity(menuResponse).build();
            } else {
                menuResponse = new MenuResponse("ok", null, menu);
            }
            return Response.ok().entity(menuResponse).build();
        } catch (Exception e) {
            LOG.error("Failed to load menu list");
            menuResponse = new MenuResponse("error", e.getMessage(), null);
            return Response.status(Response.Status.BAD_REQUEST).entity(menuResponse).build();
        }
    }
}
