package com.alexrnv.oddlemenutest.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "menus")
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "com.alexrnv.oddlemenutest.entity.Menu.findById",
                query = "select * from menus where menu_id = :menu_id",
                resultClass = Menu.class
        )
})
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "menuId", "name", "categories" })
public class Menu implements Serializable {

    private int id;
    private String name;
    private List<Category> categories = new ArrayList<>();

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Id
    @Column(name = "menu_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(value = "menuId")
    public int getId() {
        return id;
    }

    @Column(name = "name")
    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    @OneToMany
    @JoinColumn(name = "menu_id")
    @JsonProperty(value = "categories")
    public List<Category> getCategories() {
        return categories;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
