package com.alexrnv.oddlemenutest.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "subitems")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "subitemId", "name", "price", "display_position" })
public class SubItem implements Serializable {

    private int id;
    private String name;
    private double price;
    private int displayPosition;
    private List<VariantSubItem> variantSubItems = new ArrayList<>();

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDisplayPosition(int displayPosition) {
        this.displayPosition = displayPosition;
    }

    public void setVariantSubItems(List<VariantSubItem> variantSubItems) {
        this.variantSubItems = variantSubItems;
    }

    @Id
    @Column(name = "subitem_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(value = "subitemId")
    public int getId() {
        return id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Column(name = "price")
    public double getPrice() {
        return price;
    }

    @OneToMany(mappedBy = "subItem")
    @OrderBy("displayPosition")
    @JsonIgnore
    public List<VariantSubItem> getVariantSubItems() {
        return variantSubItems;
    }

    @Transient
    @JsonProperty(value = "display_position")
    public int getDisplayPosition() {
        return displayPosition;
    }

    @Override
    public String toString() {
        return "SubItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
