package com.alexrnv.oddlemenutest.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Many-to-many relation table has extra field, so we cannot just use {@link ManyToMany} annotation and will
 * treat it as separate entity.
 */
@Entity
@Table(name = "category_item")
public class CategoryItem implements Serializable {

    private int id;
    private Category category;
    private Item item;
    private int displayPosition;

    public void setId(int id) {
        this.id = id;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void setDisplayPosition(int displayPosition) {
        this.displayPosition = displayPosition;
    }

    @Id
    @Column(name = "category_item")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "category_id")
    public Category getCategory() {
        return category;
    }

    @ManyToOne
    @JoinColumn(name = "item_id")
    public Item getItem() {
        return item;
    }

    @Column(name = "display_position")
    public int getDisplayPosition() {
        return displayPosition;
    }

    @Override
    public String toString() {
        return "CategoryItem{" +
                "category=" + category +
                ", item=" + item +
                ", displayPosition=" + displayPosition +
                '}';
    }
}
