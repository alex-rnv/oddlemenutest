package com.alexrnv.oddlemenutest.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 */
@Entity
@Table(name = "variants")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "variantId", "name", "price", "subitems", "display_position" })
public class Variant implements Serializable {

    private int id;
    private String name;
    private double price;
    private List<VariantSubItem> variantSubItems = new ArrayList<>();

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setVariantSubItems(List<VariantSubItem> variantSubItems) {
        this.variantSubItems = variantSubItems;
    }

    @Id
    @Column(name = "variant_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(value = "variantId")
    public int getId() {
        return id;
    }

    @Column(name = "name")
    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    @Column(name = "price")
    @JsonProperty(value = "price")
    public double getPrice() {
        return price;
    }

    @OneToMany(mappedBy = "variant")
    @OrderBy("displayPosition")
    @JsonIgnore
    public List<VariantSubItem> getVariantSubItems() {
        return variantSubItems;
    }

    @Transient
    @JsonProperty(value = "subitems")
    public List<SubItem> getSubItems() {
        return getVariantSubItems().stream().map(vs -> {
            SubItem subItem = vs.getSubItem();
            //display position for subitems is defined by current variant
            subItem.setDisplayPosition(vs.getDisplayPosition());
            return subItem;

        }).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Variant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
