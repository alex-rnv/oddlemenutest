package com.alexrnv.oddlemenutest.entity;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "categories")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "categoryId", "name", "items", "display_position" })
public class Category implements Serializable {

    private int id;
    private String name;
    private int displayPosition;
    private List<CategoryItem> categoryItems;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDisplayPosition(int displayPosition) {
        this.displayPosition = displayPosition;
    }

    public void setCategoryItems(List<CategoryItem> categoryItems) {
        this.categoryItems = categoryItems;
    }

    @Id
    @Column(name = "category_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(value = "categoryId")
    public int getId() {
        return id;
    }

    @Column(name = "name")
    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    @Column(name = "display_position")
    @JsonProperty(value = "display_position")
    public int getDisplayPosition() {
        return displayPosition;
    }

    @OneToMany(mappedBy = "category")
    @OrderBy("displayPosition")
    @JsonIgnore
    public List<CategoryItem> getCategoryItems() {
        return categoryItems;
    }

    @Transient
    @JsonProperty(value = "items")
    private List<Item> getItems() {
        return getCategoryItems().stream().map(ci -> {
            Item item = ci.getItem();
            //display position for items is defined by category-item mapping
            item.setDisplayPosition(ci.getDisplayPosition());
            return item;
        }).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", displayPosition=" + displayPosition +
                '}';
    }
}
