package com.alexrnv.oddlemenutest.entity;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "items")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "itemId", "name", "variants", "display_position" })
public class Item implements Serializable {

    private int id;
    private String name;
    private int displayPosition;
    private List<CategoryItem> categoryItems = new ArrayList<>();
    private List<Variant> variants = new ArrayList<>();

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDisplayPosition(int displayPosition) {
        this.displayPosition = displayPosition;
    }

    public void setCategoryItems(List<CategoryItem> categoryItems) {
        this.categoryItems = categoryItems;
    }

    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    @Id
    @Column(name = "item_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(value = "itemId")
    public int getId() {
        return id;
    }

    @Column(name = "name")
    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    @OneToMany(mappedBy = "item")
    @OrderBy("displayPosition")
    @JsonIgnore
    public List<CategoryItem> getCategoryItems() {
        return categoryItems;
    }

    @OneToMany
    @JoinColumn(name = "item_id")
    @JsonProperty(value = "variants")
    public List<Variant> getVariants() {
        return variants;
    }

    @Transient
    @JsonProperty(value = "display_position")
    public int getDisplayPosition() {
        return displayPosition;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
