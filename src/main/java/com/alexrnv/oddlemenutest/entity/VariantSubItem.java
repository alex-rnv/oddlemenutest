package com.alexrnv.oddlemenutest.entity;

import javax.persistence.*;

/**
 * Many-to-many relation table has extra field, so we cannot just use {@link ManyToMany} annotation and will
 * treat it as separate entity.
 */
@Entity
@Table(name = "variant_subitem")
public class VariantSubItem {

    private int id;
    private Variant variant;
    private SubItem subItem;
    private int displayPosition;

    public void setId(int id) {
        this.id = id;
    }

    public void setVariant(Variant variant) {
        this.variant = variant;
    }

    public void setSubItem(SubItem subItem) {
        this.subItem = subItem;
    }

    public void setDisplayPosition(int displayPosition) {
        this.displayPosition = displayPosition;
    }

    @Id
    @Column(name = "variant_subitem_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "variant_id")
    public Variant getVariant() {
        return variant;
    }

    @ManyToOne
    @JoinColumn(name = "subitem_id")
    public SubItem getSubItem() {
        return subItem;
    }

    @Column(name = "display_position")
    public int getDisplayPosition() {
        return displayPosition;
    }

    @Override
    public String toString() {
        return "VariantSubItem{" +
                "variant=" + variant +
                ", subItem=" + subItem +
                ", displayPosition=" + displayPosition +
                '}';
    }
}
