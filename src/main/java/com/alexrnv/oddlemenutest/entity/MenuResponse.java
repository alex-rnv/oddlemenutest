package com.alexrnv.oddlemenutest.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * Encapsulates api response data.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "status" })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MenuResponse implements Serializable {

    private String status;
    private String errorMsg;
    private Menu menu;

    public MenuResponse(String status, String errorMsg, Menu menu) {
        this.status = status;
        this.menu = menu;
        this.errorMsg = errorMsg;
    }

    @JsonProperty(value = "status")
    public String getStatus() {
        return status;
    }

    @JsonProperty(value = "error_message")
    public String getErrorMsg() {
        return errorMsg;
    }

    @JsonProperty(value = "menu")
    public Menu getMenu() {
        return menu;
    }
}
