package com.alexrnv.oddlemenutest;

import com.alexrnv.oddlemenutest.controller.MenuController;
import com.alexrnv.oddlemenutest.dao.MenuDAO;
import com.alexrnv.oddlemenutest.entity.*;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

public class OddleMenuApplication extends Application<MenuAppConfiguration>{

    private static final Logger LOG = getLogger(OddleMenuApplication.class);

    public static void main(String[] args) throws Exception {
        new OddleMenuApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<MenuAppConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/html", ""));
        bootstrap.addBundle(hibernate);
    }

    @Override
    public void run(MenuAppConfiguration configuration, Environment environment) throws Exception {
        MenuDAO menuDAO = new MenuDAO(hibernate.getSessionFactory());
        MenuController menuController = new MenuController(menuDAO);
        environment.jersey().register(menuController);
    }

    private final HibernateBundle<MenuAppConfiguration> hibernate =
            new HibernateBundle<MenuAppConfiguration>(
                    Category.class, CategoryItem.class, Item.class, Menu.class,
                    SubItem.class, Variant.class, VariantSubItem.class) {

                @Override
                public DataSourceFactory getDataSourceFactory(MenuAppConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }

                @Override
                protected Hibernate5Module createHibernate5Module() {
                    Hibernate5Module module = super.createHibernate5Module();
                    //should not ignore @transient
                    module.disable(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION);
                    return module;
                }
            };
}
