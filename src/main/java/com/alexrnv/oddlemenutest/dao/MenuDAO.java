package com.alexrnv.oddlemenutest.dao;

import com.alexrnv.oddlemenutest.entity.Menu;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

public class MenuDAO extends AbstractDAO<Menu> {

    public MenuDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Menu findById(int menuId) {
        return uniqueResult(
                namedQuery("com.alexrnv.oddlemenutest.entity.Menu.findById")
                        .setInteger("menu_id", menuId)
        );
    }
}
